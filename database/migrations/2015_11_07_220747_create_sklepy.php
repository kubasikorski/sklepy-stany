<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSklepy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('towary', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('ean')->unique();
            $table->string('nazwa_towaru');
            $table->timestamps();
        });

        Schema::create('stany', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('ean');
            $table->integer('shop_id');
            $table->integer('ilosc');
            $table->decimal('cena',10,2);
            $table->index(['ean', 'shop_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('towary');
        Schema::drop('stany');
    }
}
