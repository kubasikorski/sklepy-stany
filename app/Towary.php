<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Towary extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'towary';
    protected $fillable = array('ean');
}
