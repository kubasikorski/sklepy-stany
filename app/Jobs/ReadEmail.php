<?php

namespace App\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use PhpImap\Mailbox;

class ReadEmail implements SelfHandling
{
    /**
     * The mailer implementation.
     */
    protected $mailer;

    /**
     * Create a new instance.
     *
     */
    public function __construct()
    {

    }

    /**
     *
     *
     *
     */
    public function getMailBox()
    {
        $mailbox = new \PhpImap\Mailbox('{' . \Config::get('app.imap_host') . ':' . \Config::get('app.imap_port') . '/imap}INBOX', \Config::get('app.imap_login'), \Config::get('app.imap_password'), __DIR__);
        return $mailbox;
        //
    }

    /**
     *
     *
     * return @array
     */
    public function getMailIds(Mailbox $mailbox)
    {
        $mailsIds = $mailbox->searchMailBox('ALL');
        return $mailsIds;
    }

    /**
     *
     *
     *
     */
    public function processMails(Mailbox $mailbox, $mailsIds)
    {
        // tablica do zbierania gotowych danych
        // petla po wszystkich listach w skrzynce

        foreach ($mailsIds as $mailId) {
            $mail = $mailbox->getMail($mailId);
            // jesli sa zalaczniki
            $attachments = $mail->getAttachments();
            if (is_array($attachments)) {
                //
                // interesuje nas tylko jeden zalacznik
                // w tym miejscu moze tez byc petla po wszystkich zalacznikach - to zalezy od konstrukcji maila
                $attachmentsId = reset($attachments);
                $this->checkAttachments($attachmentsId);
                return true;
            } else {
                // nie ma zalacznikow
                return false;
            }
        }
    }

    /**
     *
     *
     *
     */
    public function checkAttachments($attachmentsId)
    {
        // sprawdzamy czy typ zalacznika jest tekstowy
        if (mime_content_type($attachmentsId->filePath) == 'text/plain') {
            // procesujemy zalacznik
            $processedData = $this->processAttachment($attachmentsId);
            // usuwamy plik tymczasowy
            unlink($attachmentsId->filePath);
            // robimy magie z danymi - obsluga w bazie
            $this->makeDatabaseMagic($processedData);
            return true;
        } else {
            // tu wyjatek jesli nie jest
            return false;
        }
    }

    /**
     *
     *
     *
     */
    public function processAttachment($attachmentsId)
    {
        $processedData = [];
        $rawfile = file_get_contents($attachmentsId->filePath);
        $rawfile = iconv("CP852", "UTF-8", $rawfile);
        $rawdata = preg_split("/\n/", $rawfile);
        // ustaw domyslne kodowanie dla mb
        mb_internal_encoding("UTF-8");
        // dlugosc pierwszego rzedu - do nieprzetwarzania pustych linii z txt (ostatnich)
        $first_row_len = mb_strlen($rawdata[0]);
        // slownik pierwszych znakow naglowkow pseudokolumn
        $colnames = array("KOD KRESKOWY", "Nazwa towaru", "JM", "Ilość", "Cena", "Wartość", "stawka", "wartość", "Marża");
        $colcount = count($colnames);
        $position = [];
        // pozycje pierwszych znakow naglowkow pseudokolumn
        foreach ($colnames as $poscount => $possearch) {
            $position[$poscount] = mb_strpos($rawdata[0], $possearch);
        }
        // dla kazdego klucza tablicy tymczasowej poza pierwszym,
        foreach (array_slice($rawdata, 1) as $rowcount => $row) {
            // jesli jego dlugosc jest wieksza od polowy dlugosci naglowka,
            if (mb_strlen($row) > $first_row_len / 2) {
                // odnajdz dane miedzy pozycjami, usun zbedne spacje i wrzuc do nowej tablicy
                foreach ($position as $itemcount => $number) {
                    $processedData[$rowcount][$itemcount] = trim(mb_substr($row, $position[$itemcount], ($itemcount < $colcount - 1 ? $position[$itemcount + 1] - $position[$itemcount] : $position[$itemcount])), " ");
                }
            }
        }
        return $processedData;
    }
    /**
     *
     *
     *
     */
    public function makeDatabaseMagic($processedData)
    {
        foreach($processedData as $towar){
            // odswiezamy baze towarow
            $towary = \App\Towary::firstOrNew(['ean' => $towar[0]]);
            $towary->ean = $towar[0];
            $towary->nazwa_towaru = $towar[1];
            $towary->save();
            // aktualizujemy stan w sklepie nr 1
            // odswiezamy baze towarow
            $stany = \App\Stany::firstOrNew(['ean' => $towar[0],'shop_id'=>1]);
            $stany->ean = $towar[0];
            $stany->shop_id = 1;
            $stany->ilosc = $towar[3];
            $stany->cena = $towar[4];
            $stany->save();
        }
    }
}