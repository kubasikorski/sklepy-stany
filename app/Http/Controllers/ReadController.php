<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ReadEmail;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReadController extends Controller
{
    /**
     *
     */
    public function readMailbox()
    {
        //
        $ReadEmail = new ReadEmail();
        //
        // powolujemy polaczenie ze skrzynka mailowa
        try {
            $mailbox = $ReadEmail->getMailBox();
        } catch (Exception $e) {
            throw $e;
        }
        // pobieramy liste maili w skrzynce
        $mailsIds = $ReadEmail->getMailIds($mailbox);
        if ($mailsIds) {
            // jesli w skrzynce leza listy
            $result = $ReadEmail->processMails($mailbox, $mailsIds);
            return response()->json(['status' => 'success']);
        } else {
            // jesli nie ma listow w skrzynce
            return response()->json(['status' => 'mailbox_empty']);
        }
    }
}
