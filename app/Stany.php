<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stany extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stany';
    protected $fillable = array('ean','shop_id');
}
