<?php

namespace App\Providers;
use App\Jobs\ReadEmail;
use Illuminate\Support\ServiceProvider;


class ReadEmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ReadEmail', function ($app) {
            return new ReadEmail();
        });
    }
}
